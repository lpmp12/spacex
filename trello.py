import requests
from words import give_a_word
from random import randint

class Trello:
	def __init__(self, api_key, api_token, board_id):
		self.api_key = api_key
		self.api_token = api_token
		self.base_url = 'https://api.trello.com/1'
		self.board_id = board_id

	def request(self, method, endpoint, **params):
		base = {
			'key': self.api_key,
			'token': self.api_token,
			**params
		}
		URL = f"{self.base_url}/{endpoint}"
		return requests.request(method, URL, params=base)	

	def get_todo_list(self):
		req = self.request('GET', f"/boards/{self.board_id}/lists")
		todo_list = filter(lambda lst: lst['name'] == 'To Do', req.json())
		return list(todo_list)[0]

	def get_or_create_label(self, name):
		labels = self.request('GET', f"/boards/{self.board_id}/labels")
		search = list(filter(lambda x: x == name, labels.json()))
		if not search:
			board_identifier = self.request('GET', f'/board/{self.board_id}').json()['id']
			return self.request('POST', '/labels', **{"name": name, "idBoard": board_identifier }).json()['id']
		return search[0]['id']

	def get_params(self, todo_list, card_type, params):
		bug_title = f"bug-{give_a_word()}-{randint(1000,9999)}"
		return {
			"bug": {"name": bug_title, 
					"description": params['description'], 
					"idList": todo_list},
			"issue": {"name": params['title'], 
					"description": params['description'], 
					"idList": todo_list},
			"task": {"name": params['title'], 
					"idLabels": self.get_or_create_label(params['category']) if card_type == "task" else "", 
					"idList": todo_list}
		}[card_type]

	def create_card(self, card_type, params):
		todo = self.get_todo_list()
		params = self.get_params(todo['id'], card_type, params)
		return self.request('POST', '/cards', **params)