from typing import Optional
from pydantic import BaseModel, ValidationError, validator
from trello import Trello

class CommonTask(BaseModel):
	type: str
	title: Optional[str]
	description: Optional[str]
	category: Optional[str]

	@validator('type')
	def type_exists(cls, v):
		if v not in ['issue', 'bug', 'task']:
			raise ValueError('Task type not recognized.')
		return v

	def save(self):
		# this should be set in an env variable, not here. future improvement. 
		trello = Trello("432bc17dc60f69d41074dd53935e7d4d", "181da5480bf63462e398e07d907ed0051d72f18940d2ac007c1d8617622428a6", 'cRNKKRBR')
		return trello.create_card(self.type, self.__dict__)


class Task(CommonTask):
	title: str
	category: str

	@validator('category')
	def category_exists(cls, v):
		if v not in ["Maintenance", "Research", "Test"]:
			raise ValueError('Category type not recognized.')
		return v


class Issue(CommonTask):
	title: str
	description: str


class Bug(CommonTask):
	description: str