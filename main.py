from fastapi import FastAPI
from models import *

app = FastAPI()

relations = {
	'issue': Issue,
	'bug': Bug,
	'task': Task
}

@app.post("/")
async def record(task: CommonTask):
	try:
		model = relations[task.type]
		validate = model(**task.dict())
		validate.save()
		return validate.dict()
	except ValidationError as e:
		return e.errors()